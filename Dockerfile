# базовый образ Alpine Linux
FROM alpine:latest
COPY . /app
WORKDIR /app
RUN apk add --no-cache openssl-dev build-base
CMD ["openssl"]
RUN ls /tmp
