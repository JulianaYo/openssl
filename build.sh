#!/bin/bash

while getopts ":bt" opt; do
  case $opt in
    b)
      make build_openssl
      ;;
    t)
      make test_openssl
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done